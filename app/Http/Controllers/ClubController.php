<?php

namespace App\Http\Controllers;

use App\Club;
use App\Player;
use App\PlayerGroup;
use App\Team;
use App\User;
use Illuminate\Http\Request;

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clubList = Club::all();

        $description = "Request For Club List ";

        activity()->log($description);


        return view('club.club_list',compact('clubList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('club.add_club');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $club = new Club;
        $club->name = $request->name;
        $club->save();

        $description = "New Club '".$request->name."' added";

        activity()->log($description);
        
        return redirect(route('club.index'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Club',
                  'message' => 'Club Successfully added.',
              ],
        ]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getClubInfo = Club::find($id);

        $description = "Edit Request for club '".$groupInfo->name."'";

        activity()->log($description);

        return view('club.edit_club',compact('getClubInfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $club = Club::findOrFail($id);
        $club->name = $request->name;
        $club->save();

        $description = "Club Updated ".$request->name;

        activity()->log($description);
        
        return redirect(route('club.index'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Club',
                  'message' => 'Club Successfully updated.',
              ],
        ]); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $club = Club::where('id',$id)->delete();

        $deleteUser = User::where('club_id',$id)->delete();
        $team = Team::where('id',$id)->pluck('id');
        $group = PlayerGroup::whereIn('team_id',$team)->pluck('id');
        $deltePlayer = Player::whereIn('group_id',$group)->delete();
        $deletegroup = PlayerGroup::whereIn('team_id',$team)->delete();
        $deleteteam = Team::where('id',$id)->delete();

        $description = "Club #".$id." deleted";

        activity()->log($description);

        return redirect(route('club.index'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Club',
                  'message' => 'Club Successfully deleted.',
              ],
        ]); 
    }
}
