<?php

namespace App\Http\Controllers;

use App\Club;
use App\Jobs\SendWelcomMail;
use App\User;
use App\UserClub;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class ClubAdminController extends GlobalController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clubAdminList = User::with(['userclub'])->get();

        $description = "Request For Clubadmin List ";

        activity()->log($description);

        return view('clubadmin.club_admin',compact('clubAdminList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clubList = Club::all();

        return view('clubadmin.add_club_admin',compact('clubList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
            'club_id' => 'required',
            'picture' => 'required|image',
        ]);

        $user = new User;
        $user->name = $request->name;
        $user->club_id = $request->club_id;
        $user->email = $request->email;
        if(isset($request->super_admin)){
            $user->isAdmin = 1;
        }
        if(isset($request->picture)){
            $filename = $this->uploadImage($request->picture,'picture');
            $user->picture = $filename;
        }
        $user->password = Hash::make($request->password);
        $user->save();

        $description = "New Club '".$request->name."' added";

        activity()->log($description);

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['password'] = $request->password;

        dispatch(new SendWelcomMail($data,$request->email))->delay(Carbon::now()->addSeconds(5));

        return redirect(route('clubadmin.index'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Club',
                  'message' => 'Club Successfully added.',
              ],
        ]); 
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $clubList = Club::all();

        $getClubInfo = User::find($id);

        $description = "Edit Request for clubadmin '".$getClubInfo->name."'";

        activity()->log($description);

        return view('clubadmin.edit_club_admin',compact('getClubInfo','clubList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'club_id' => 'required',
        ]);

        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->club_id = $request->club_id;
        $user->email = $request->email;
        if(isset($request->super_admin)){
            $user->isAdmin = 1;
        } else {
            $user->isAdmin = 0;
        }
        if(isset($request->picture)){
            $filename = $this->uploadImage($request->picture,'picture');
            $user->picture = $filename;
        }
        $user->password = Hash::make($request->password);
        $user->save();

        $description = "Clubadmin Updated ".$request->name;

        activity()->log($description);

        return redirect(route('clubadmin.index'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Club',
                  'message' => 'Club Successfully Updated.',
              ],
        ]); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $club = User::where('id',$id)->delete();

        $description = "Clubadmin #".$id." deleted";

        activity()->log($description);

        return redirect(route('club.index'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Club',
                  'message' => 'Clubadmin Successfully deleted.',
              ],
        ]); 
    }
}
