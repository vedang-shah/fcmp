<div id="sidebar" class="sidebar">
	<!-- begin sidebar scrollbar -->
	<div data-scrollbar="true" data-height="100%">
		<!-- begin sidebar nav -->
		<ul class="nav">
			<li class="@if(route::is('home')) active @endif">
				<a href="javascript:void(0);">
				    <i class="fa fa-laptop"></i>
				    <span>Dashboard</span>
			    </a>
			</li>

			@can('admin-only', auth()->user())
				<li class="has-sub @if(route::is('club.create') || route::is('club.index')) active @endif">
					<a href="javascript:;">
					    <b class="caret pull-right"></b>
					    <i class="fa fa-building"></i>
					    <span>Club</span>
				    </a>
					<ul class="sub-menu">
					    <li class="@if(route::is('club.create'))active @endif"><a href="{{ route('club.create') }}">Add Club</a></li>
					    <li class="@if(route::is('club.index'))active @endif"><a href="{{ route('club.index') }}">Club List</a></li>
					</ul>
				</li>

				<li class="has-sub @if(route::is('clubadmin.create') || route::is('clubadmin.index')) active @endif">
					<a href="javascript:;">
					    <b class="caret pull-right"></b>
					    <i class="fa fa-user"></i>
					    <span>Club Admin</span>
				    </a>
					<ul class="sub-menu">
					    <li class="@if(route::is('clubadmin.create'))active @endif"><a href="{{ route('clubadmin.create') }}">Add Club Admin</a></li>
					    <li class="@if(route::is('clubadmin.index'))active @endif"><a href="{{ route('clubadmin.index') }}">Club Admin List</a></li>
					</ul>
				</li>

			@endcan

			<li class="has-sub @if(route::is('group.create') || route::is('group.index')) active @endif">
				<a href="javascript:;">
				    <b class="caret pull-right"></b>
				    <i class="fa fa-user"></i>
				    <span>Player Group</span>
			    </a>
				<ul class="sub-menu">
				    <li class="@if(route::is('group.create'))active @endif"><a href="{{ route('group.create') }}">Add Player Group</a></li>
				    <li class="@if(route::is('group.index'))active @endif"><a href="{{ route('group.index') }}">Player Group List</a></li>
				</ul>
			</li>

			<li class="has-sub @if(route::is('team.create') || route::is('team.index')) active @endif">
				<a href="javascript:;">
				    <b class="caret pull-right"></b>
				    <i class="fa fa-user"></i>
				    <span>Team</span>
			    </a>
				<ul class="sub-menu">
				    <li class="@if(route::is('team.create'))active @endif"><a href="{{ route('team.create') }}">Add Team Admin</a></li>
				    <li class="@if(route::is('team.index'))active @endif"><a href="{{ route('team.index') }}">Team List</a></li>
				</ul>
			</li>

			<li class="has-sub @if(route::is('player.create') || route::is('player.index')) active @endif">
				<a href="javascript:;">
				    <b class="caret pull-right"></b>
				    <i class="fa fa-user"></i>
				    <span>Player</span>
			    </a>
				<ul class="sub-menu">
				    <li class="@if(route::is('player.create'))active @endif"><a href="{{ route('player.create') }}">Add Player</a></li>
				    <li class="@if(route::is('player.index'))active @endif"><a href="{{ route('player.index') }}">Player List</a></li>
				</ul>
			</li>
			
		</ul>
		<!-- end sidebar nav -->
	</div>
	<!-- end sidebar scrollbar -->
</div>