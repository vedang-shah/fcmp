@extends('layouts.admin')
@section('title','Team List')
@section('content')
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="{{ route('home') }}">Home</a></li>
        <li class="active">Team List</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Team List</h1>
    <!-- end page-header -->
    <!-- begin row -->
    <div class="row">
        <!-- begin col-10 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Team List</h4>
                </div>
                
                <div class="panel-body">
                    <table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
                        <thead>
                            <tr>
                                <th>Sr.no</th>
                                <th>Team Name</th>
                                <th>Club Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($team) > 0)
                            @foreach($team as $tk => $tv)   
                                <tr class="odd gradeX">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $tv->name}}</td>
                                    <td>{{ $tv['club']['name']}}</td>
                                    <td>
                                        <a href="{{ route('team.edit',$tv->id) }}" class="btn btn-info btn-icon btn-circle btn-lg" style="float: left;"><i class="fa fa-edit" ></i></a>
                                        <form  method="post" action="{{ route('team.destroy',$tv->id) }}">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-icon btn-circle btn-lg" onclick="return confirm('Are you sure want to delete this team ?')"><i class="fa fa-times"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>
@endsection