<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    public function group(){
        return $this->hasOne('App\PlayerGroup','id','group_id');
    }
}
