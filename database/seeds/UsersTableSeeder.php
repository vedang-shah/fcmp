<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Vedang',
            'isAdmin' => 1,
            'email' => 'vedang@admin.com',
            'password' => bcrypt('admin123'),
        ]);
    }
}
