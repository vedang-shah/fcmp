@extends('layouts.admin')
@section('title','Club Admin List')
@section('content')
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="{{ route('home') }}">Home</a></li>
        <li class="active">club admin List</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">club admin List</h1>
    <!-- end page-header -->
    <!-- begin row -->
    <div class="row">
        <!-- begin col-10 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">club admin List</h4>
                </div>
                
                <div class="panel-body">
                    <table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
                        <thead>
                            <tr>
                                <th>Sr.no</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Club Name</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($clubAdminList) > 0)
                            @foreach($clubAdminList as $ck => $cv)   
                                <tr class="odd gradeX">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $cv->name}}</td>
                                    <td>{{ $cv->email}}</td>
                                    @if(!is_null($cv->userclub))
                                        <td>{{ $cv->userclub->name }}</td>
                                    @else
                                        <td>---------</td>
                                    @endif
                                    <td>{{ $cv->isAdmin ? 'Super Admin' : 'Club Admin'}}</td>
                                    <td>
                                        <a href="{{ route('clubadmin.edit',$cv->id) }}" class="btn btn-info btn-icon btn-circle btn-lg" style="float: left;"><i class="fa fa-edit"></i></a>
                                        <form  method="post" action="{{ route('clubadmin.destroy',$cv->id) }}">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-icon btn-circle btn-lg" onclick="return confirm('Are you sure want to delete this club admin ?')"><i class="fa fa-times"></i>
                                            </button>
                                        </form><br>
                                        @canBeImpersonated($cv)
                                            <a href="{{ route('impersonate', $cv->id) }}" class="btn btn-warning " style="float: left;">Impersonate</a>
                                        @endCanImpersonate
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>
@endsection