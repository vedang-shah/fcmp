@extends('layouts.admin')
@section('title','Edit Club')
@section('content')
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li><a href="{{ route('home') }}">Home</a></li>
		<li class="active">Edit Club</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Club Admin</h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
        <!-- begin col-6 -->
	    <div class="col-md-12">
	        <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
                <div class="panel-heading">
                    <h4 class="panel-title">Edit Club</h4>
                </div>
                 @if ($errors->any())
                    @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                    @endforeach
                @endif
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="{{ route('clubadmin.update',$getClubInfo->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label class="col-md-3 control-label">Select Club</label>
                            <div class="col-md-6">
                                <select class="form-control" name="club_id">
                                    <option value="">Select Club</option>
                                    @if(count($clubList) > 0)
                                        @foreach($clubList as $ck => $cv)
                                            <option value="{{ $cv->id }}" @if($getClubInfo->club_id == $cv->id) selected="selected" @endif>{{ $cv->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" placeholder="Enter Name" value="{{ $getClubInfo->name }}" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Email</label>
                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ $getClubInfo->email }}" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Picture</label>
                            <div class="col-md-6">
                                <input type="file" name="picture" class="form-control dropify" data-default-file="{{ asset('uploads/picture') }}/{{ $getClubInfo->picture }}"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="checkbox" name="super_admin" value="1" @if($getClubInfo->isAdmin == 1) checked @endif>&nbsp;&nbsp;
                                <h8>Make Super Admin</h8>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
    </div>
    <!-- end row -->
</div>
@endsection
