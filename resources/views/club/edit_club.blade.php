@extends('layouts.admin')
@section('title','Edit Club')
@section('content')
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li><a href="{{ route('home') }}">Home</a></li>
		<li class="active">Edit Club</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Club</h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
        <!-- begin col-6 -->
	    <div class="col-md-12">
	        <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
                <div class="panel-heading">
                    <h4 class="panel-title">Edit Club</h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="{{ route('club.update',$getClubInfo->id) }}" enctype="multipart/form-data">
                        @csrf
                        @method('put')

                        <input type="hidden" name="id" value="{{ $getClubInfo->id }}">

                        <div class="form-group">
                            <label class="col-md-3 control-label">Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" placeholder="Enter Name" value="{{ $getClubInfo->name }}" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
    </div>
    <!-- end row -->
</div>
@endsection
