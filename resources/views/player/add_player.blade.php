@extends('layouts.admin')
@section('title','Add Player')
@section('content')
<style type="text/css">
.error{
    text-align: left;
    color:red;
}
</style>
<div id="content" class="content">
	<!-- begin breadcrumb -->
	<ol class="breadcrumb pull-right">
		<li><a href="{{ route('home') }}">Home</a></li>
		<li class="active">Add Player</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">Player</h1>
	<!-- end page-header -->
	
	<!-- begin row -->
	<div class="row">
        <!-- begin col-6 -->
	    <div class="col-md-12">
	        <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
                <div class="panel-heading">
                    <h4 class="panel-title">Add Player</h4>
                </div>
                 @if ($errors->any())
                    @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                        {{ $error }}
                    </div>
                    @endforeach
                @endif
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="{{ route('player.store') }}" id="addPlayer" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label class="col-md-3 control-label">Group Name</label>
                            <div class="col-md-6">
                                <select class="form-control" name="group_id" required="required" id="group_id">
                                    <option value="" selected="">Select Group</option>
                                    @foreach($group as $groups)
                                        <option value="{{$groups->id}}">{{$groups->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Player Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" placeholder="Enter Player Name" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Player Photo</label>
                            <div class="col-md-6">
                                <input type="file" name="photo" class="form-control dropify" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
    </div>
    <!-- end row -->
</div>
@endsection
