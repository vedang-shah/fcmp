<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::resources([
    'club' => 'ClubController',
    'clubadmin' => 'ClubAdminController',
    'group' => 'GroupController',
    'team' => 'TeamController',
    'player' => 'PlayerController'
]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/private', 'HomeController@superadmin')->name('superadmin');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::post('check-player', 'PlayerController@checkPlayer')->name('checkPlayer');

Route::impersonate();