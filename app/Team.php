<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public function club(){
        return $this->hasOne('App\Club','id','club_id');
    }
}
