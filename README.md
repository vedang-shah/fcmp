#Project Installation Step

Rename .env.example file => .env

1) Clone project from git repository

2) Run the command composer install

3) Set your database name, databsae username and password in .env file 

4) Set mailtrap credentials in .env file

5) Run the command "php artisan migrate"

6) Then run the command "php artisan db:seed"

7) Generate the key: php artisan key:generate

8) Run the project - php artisan serve

9) super admin credentials will be "email:vedang@admin.com and password: admin123"

10) Run this command for events - php artisan queue:work --queue=high,default