$(document).ready(function() {

    //custom validation method
    $.validator.addMethod("customemail", 
        function(value, element) {
            return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
        }, 
        "Please enter email id along with domain name."
    );

    $.validator.addMethod('decimal', function(value, element) {
      return this.optional(element) || /^((\d+(\\.\d{0,2})?)|((\d*(\.\d{1,2}))))$/.test(value);
    }, "Please enter a correct size");

    // validate role form
    $("#roleForm").validate({
        errorElement: 'span',
        rules: {
            name: {
                required: true,
            },
        },
        messages: {
            name:"Please enter role name",
        }
    });
    
    //validate user form
    $("#addPlayer").validate({
        errorElement: 'span',
        rules: {
            group_id: {
                required: true
            },
            name: {
                required: true,
                remote:{
                    url: "/check-player",
                    type: "post",
                    data: {
                        group_id: function() {
                            return $( "#group_id" ).val();
                        },
                    }
                }
            }
        },
        messages: {
            group_id:{
                required:"Please select group",
            },
            name:{
                required:"Please enter player name",
                remote:"This player already assigned!"
            }
        }
    });

     //validate user form
    $("#editPlayer").validate({
        errorElement: 'span',
        rules: {
            group_id: {
                required: true
            },
            name: {
                required: true,
                remote:{
                    url: "/check-player",
                    type: "post",
                    data: {
                        id: function() {
                            return $( "#id" ).val();
                        },
                    }
                }
            }
        },
        messages: {
            group_id:{
                required:"Please select group",
            },
            name:{
                required:"Please enter player name",
                remote:"This player already assigned!"
            }
        }
    });

   
});
    