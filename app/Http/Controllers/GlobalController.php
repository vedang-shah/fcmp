<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GlobalController extends Controller
{
    public function uploadImage($image,$path){
       $imagedata = $image;
       $destinationPath = 'uploads/'.$path; 
       $extension = $imagedata->getClientOriginalExtension(); 
       $fileName = rand(11111,99999).'.'.$extension;
       $imagedata->move($destinationPath, $fileName);
       
       return $fileName;
   	}
}
