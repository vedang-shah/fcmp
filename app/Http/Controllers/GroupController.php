<?php

namespace App\Http\Controllers;

use App\PlayerGroup;
use App\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $team = Team::where('club_id',Auth::user()->club_id)->pluck('id');

        $group = PlayerGroup::whereIn('team_id',$team)->get();

        $description = "Request For Group List ";

        activity()->log($description);

        return view('group.group_list',compact('group'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $team = Team::where('club_id',Auth::user()->club_id)->get();

        return view('group.add_group',compact('team'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'team_id' => 'required'
        ]);

        $group = new PlayerGroup;
        $group->name = $request->name;
        $group->team_id = $request->team_id;
        $group->save();

        $description = "New Group '".$request->name."' added";

        activity()->log($description);
        
        return redirect(route('group.index'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Group',
                  'message' => 'Group Successfully added.',
              ],
        ]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $team = Team::where('club_id',Auth::user()->club_id)->get();

        $groupInfo = PlayerGroup::find($id);

        $description = "Edit Request for group'".$groupInfo->name."'";

        activity()->log($description);

        return view('group.edit_group',compact('groupInfo','team'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'team_id' => 'required'
        ]);

        $group = PlayerGroup::findOrFail($id);
        $group->name = $request->name;
        $group->team_id = $request->team_id;
        $group->save();

        $description = "Group Updated ".$request->name;

        activity()->log($description);
        
        return redirect(route('group.index'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Group',
                  'message' => 'Group Successfully Updated.',
              ],
        ]); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = PlayerGroup::where('id',$id)->delete();

        $description = "Group #".$id." deleted";

        activity()->log($description);

        return redirect(route('group.index'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Group',
                  'message' => 'Group Successfully deleted.',
              ],
        ]); 
    }
}
