<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('dashboard.dashboard');
    }

    public function superadmin()
    {

       if (Gate::allows('admin-only', auth()->user())) {
            return view('private');
        }
        return 'You are not admin!!!!'; 
    }

    public function impersonate($user_id){

        $user = User::find($user_id);
        Auth::user()->impersonate($user);

        return redirect('/dashboard');
    }

    public function impersonate_leave(){

        Auth::user()->leaveImpersonation();

        return redirect()->route('/dashboard');   
    }
}
