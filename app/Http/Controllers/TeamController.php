<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Team;
use App\Club;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    public function index(){
        
        $team = Team::where('club_id',Auth::user()->club_id)->with(['club'])->get();
        
        $description = "List Team";

        activity()->log($description);

        return view('team.team_list',compact('team'));
    }

    public function create(){

        $club = Club::where('id',Auth::user()->club_id)->get();
        
        $description = "New Team";

        activity()->log($description);

        return view('team.add_team',compact('club'));    
    }

    public function store(Request $request){
        
        Validator::make($request->all(), [
            'name' => 'required|unique:teams'
        ])->validate();

        $team = new Team;
        $team->name = $request->name;
        $team->club_id = $request->club_id;
        $team->save();

        $description = "New Team '".$request->name."' added";

        activity()->log($description);

        return redirect(route('team.index'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Team',
                  'message' => 'Team Successfully added.',
              ],
        ]);
    }

    public function edit($id){

        $edit_team = Team::findOrFail($id);
        
        $club = Club::where('id',Auth::user()->club_id)->get();

        $description = "Edit Request for Team'".$edit_team->name."'";

        activity()->log($description);

        return view('team.edit_team',compact('edit_team','club'));
    }

    public function update(Request $request){
        
        $team = Team::findOrFail($request->id);
        $team->name = $request->name;
        $team->club_id = $request->club_id;
        $team->save(); 

        $description = "Update Request for Team'".$request->name."'";

        activity()->log($description);

        return redirect(route('team.index'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Team',
                  'message' => 'Team Successfully updated.',
              ],
        ]);
    }
    
    public function destroy($id){

        $team = Team::where('id',$id)->first();

        $description = "Delete Team'".$team->name."'";

        $team = Team::where('id',$id)->delete();

        return redirect(route('team.index'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Team',
                  'message' => 'Team Successfully deleted.',
              ],
        ]);
    }
    
   
}
