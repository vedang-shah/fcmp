<?php

namespace App\Http\Controllers;

use App\Player;
use App\PlayerGroup;
use App\Team;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;

class PlayerController extends GlobalController
{
    public function index(){   
        
        $team = Team::where('club_id',Auth::user()->club_id)->pluck('id');

        $group = PlayerGroup::whereIn('team_id',$team)->pluck('id');

        $player = Player::whereIn('group_id',$group)->with(['group'])->get();
          
        $description = "List Players";

        activity()->log($description);

        return view('player.player_list',compact('player'));
    }

    public function create(){

        $team = Team::where('club_id',Auth::user()->club_id)->pluck('id');

        $group = PlayerGroup::whereIn('team_id',$team)->get();

        $description = "New Player";

        activity()->log($description);

        return view('player.add_player',compact('group'));    
    }

    public function store(Request $request){
        
        Validator::make($request->all(), [
            'name' => 'required',
            'photo' => 'required'
        ])->validate();

        $player = new Player;
        $player->name = $request->name;
        $player->group_id = $request->group_id;

        if(isset($request->photo)){
            $filename = $this->uploadImage($request->photo,'photo');
            $player->photo = $filename;
        }

        $player->save();

        $description = "New Player '".$request->name."' added";

        activity()->log($description);

        return redirect(route('player.index'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Player',
                  'message' => 'Player Successfully added.',
              ],
        ]);
    }

    public function edit($id){

        $edit_player = Player::findOrFail($id);
        
        $team = Team::where('club_id',Auth::user()->club_id)->pluck('id');

        $group = PlayerGroup::whereIn('team_id',$team)->get();

        $description = "Edit Request for Player'".$edit_player->name."'";

        activity()->log($description);

        return view('player.edit_player',compact('edit_player','group'));
    }

    public function update(Request $request){
        
        $player = Player::findOrFail($request->id);
        $player->name = $request->name;
        $player->group_id = $request->group_id;

        if(isset($request->photo)){
            $filename = $this->uploadImage($request->photo,'photo');
            $player->photo = $filename;
        }

        $player->save(); 

        $description = "Update Player'".$request->name."'";

        activity()->log($description);

        return redirect(route('player.index'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Player',
                  'message' => 'Player Successfully updated.',
              ],
        ]);
    }
    
    public function destroy($id){

        $player = Player::where('id',$id)->first();

        $description = "Delete Player'".$player->name."'";

        activity()->log($description);

        $player = Player::where('id',$id)->delete();

        return redirect(route('player.index'))->with('messages', [
              [
                  'type' => 'success',
                  'title' => 'Player',
                  'message' => 'Player Successfully deleted.',
              ],
        ]);
    }

    public function checkPlayer(Request $request){

        if(isset($request->id)){
            $player = Player::where('name','LIKE','%'.$request->name.'%')->where('id','!=',$request->id)->first();
        } else {
            $player = Player::where('name','LIKE','%'.$request->name.'%')->first();
        }

        if(!is_null($player)){
            return 'false';
        } else {
            return 'true';
        }
    }
    
   
}
