@extends('layouts.admin')
@section('title','Player List')
@section('content')
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="{{ route('home') }}">Home</a></li>
        <li class="active">Player List</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Player List</h1>
    <!-- end page-header -->
    <!-- begin row -->
    <div class="row">
        <!-- begin col-10 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Player List</h4>
                </div>
                
                <div class="panel-body">
                    <table id="data-table" class="table table-striped table-bordered nowrap" width="100%">
                        <thead>
                            <tr>
                                <th>Sr.no</th>
                                <th>Player Name</th>
                                <th>Group Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($player) > 0)
                            @foreach($player as $pk => $pv)   
                                <tr class="odd gradeX">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $pv->name}}</td>
                                    <td>{{ $pv['group']['name']}}</td>
                                    <td>
                                        <a href="{{ route('player.edit',$pv->id) }}" class="btn btn-info btn-icon btn-circle btn-lg" style="float: left;"><i class="fa fa-edit" ></i></a>
                                        <form  method="post" action="{{ route('player.destroy',$pv->id) }}">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-icon btn-circle btn-lg" onclick="return confirm('Are you sure want to delete this player ?')"><i class="fa fa-times"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>
@endsection